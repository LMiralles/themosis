<div class="w-full p-6 text-white uppercase text-sm flex items-center max-w-screen-lg fixed top-0 left-1/2 transform -translate-x-1/2 z-50">
    <a href="#" class="mx-6 transition duration-300 ease-in-out font-semibold visited:text-blue-500">Accueil</a>
    <a href="#" class="mx-6 transition duration-300 ease-in-out font-semibold visited:text-blue-500">Contact</a>
    <a href="#" class="mx-6 transition duration-300 ease-in-out font-semibold visited:text-blue-500">En savoir plus</a>
</div>