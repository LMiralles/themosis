<html>
    <head>
        <title>Tailwind - themosis</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <?php wp_head(); ?>
    </head>
    <body>
        @component('components.header')          
        @endcomponent

        @component('components.footer')           
        @endcomponent
        <?php wp_footer(); ?>
    </body>
</html>