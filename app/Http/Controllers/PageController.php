<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Show the home page.
     */
    public function index()
    {
        return view('home');
    }
    public function contact()
    {
        return view('contact');
    }
}